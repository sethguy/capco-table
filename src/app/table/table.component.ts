import {Component, OnInit} from '@angular/core';

import {tableColumns} from './tableColumns';

import {TableService} from './table.service'

@Component({selector: 'app-table', templateUrl: './table.component.html', styleUrls: ['./table.component.css']})
export class TableComponent implements OnInit {

  tableColumns = tableColumns;

  rows$ = this.tableService.getRows()
  page$ = this.tableService.getPage()
  top$ = this.tableService.getTop()

  constructor(private tableService: TableService) {

    tableService.updateTableRows()

  }

  nextPage() {

    this.tableService.nextPage()
  }
  setRows(number) {
    this.tableService.updateTop(number)

  }

  previousPage() {
    this.tableService.previousPage()

  }
  ngOnInit() {}

}
