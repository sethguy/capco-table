import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'table': {
    'height': [{ 'unit': 'px', 'value': 500 }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'black' }]
  },
  'thead': {
    'overflowX': 'hidden'
  },
  'tbody': {
    'overflowX': 'hidden'
  },
  'table': {
    'width': [{ 'unit': '%H', 'value': 2 }]
  },
  'table-header-column': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'blue' }],
    'margin': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }]
  },
  'table-column-width': {
    'textOverflow': 'ellipsis',
    'width': [{ 'unit': 'px', 'value': 300 }],
    'overflow': 'hidden'
  },
  'pagination-container button': {
    'margin': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }]
  }
});
