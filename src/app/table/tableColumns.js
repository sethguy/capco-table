export const tableColumns = [
  {
    name: "name"
  },
  {
    name: "phone"
  },
  {
    name: "email"
  },
  {
    name: "company"
  },
  {
    name: "date_entry"
  },
  {
    name: "org_num"
  },
  {
    name: "address_1"
  },
  {
    name: "city"
  },
  {
    name: "zip"
  },
  {
    name: "geo"
  },
  {
    name: "pan"
  },
  {
    name: "pin"
  },
  {
    name: "id"
  },
  {
    name: "status"
  },
  {
    name: "fee"
  },
  {
    name: "guid"
  },
  {
    name: "date_exit"
  },
  {
    name: "date_first"
  },
  {
    name: "date_recent"
  },
  {
    name: "url"
  }
]