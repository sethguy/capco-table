import { Injectable } from '@angular/core';
import {ReplaySubject} from 'rxjs';


import {tableData} from './tableData';
import {tableColumns} from './tableColumns';
import { filter,map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TableService {
  public store = new ReplaySubject()
  state ={
    skip:0,
    top:5,
    page:0,
    rows:[]

  }

  public tableColumns = tableColumns
  constructor() { }

  updateTableRows(){
    var {top,skip,page} = this.state;

    this.setState({
      rows:
      tableData
      .filter((row, i) => {
        return i >= skip && i < (skip + top);
      })
    })

  }

  getRows() {

    return this.store.pipe(
      filter( (state:any)=> state.rows ),
      map( (state:any)=> state.rows )
    )
  }
  getPage() {

    return this.store.pipe(
      map( (state:any)=> state.page+1 )
    )
  }

  getTop() {

    return this.store.pipe(
      map( (state:any)=> state.top )
    )
  }

  updateTop(top) {

    this.setState({
      skip:0,
      page:0,
      top
    })
    this.updateTableRows();

  }

  nextPage() {

    var {top,skip,page} = this.state;

    var nextPage = page+1;

    const nextSkip = top * nextPage;

    if (nextSkip < tableData.length) {

      this.setState({
        skip:nextSkip,
        page:nextPage
      })

     this.updateTableRows();
    }
  }

  previousPage() {
    var {top,skip,page} = this.state;

    if (page > 0) {

      var previousPage = page-1
      var newSkip = top * previousPage

     this.setState({
      skip:newSkip,
      page:previousPage
    })
     this.updateTableRows();

    }
  }

  setState(data){

    this.state = {
      ...this.state,
      ...data
    }

    this.store.next({
      ...this.state
    })

  }
}
